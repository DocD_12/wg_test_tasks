class CircularBuffer:
    def __init__(self, n = 10):
        self.data = [None] * n
        self.size = n
        self.head = 0
        self.tail = 0

    def get_head(self):
        return self.head

    def get_tail(self):
        return self.tail

    def get_free_space(self):
        return self.size - self.head_tail()

    def get_occupied_space(self):
        return self.head_tail()

    def add(self, value):
        self.data[self.head] = value
        self.head = self.point_increment(self.head)
        if self.head == self.tail:
            self.tail = self.point_increment(self.tail)

    def get(self):
        if (self.head == self.tail):
            raise MemoryError("CircularBuffer is Empty")
        res = self.data[self.tail]
        self.tail = self.point_increment(self.tail)
        return res

    def head_tail(self):
        if (self.head < self.tail):
            return (self.head + self.size - self.tail)
        else:
            return self.head - self.tail

    def point_increment(self, point):
        if (point + 1 >= self.size):
            return 0
        else:
            return point + 1
