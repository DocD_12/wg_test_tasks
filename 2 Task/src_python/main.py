from CircularBuffer import CircularBuffer
from CircularBuffer2 import CircularBuffer2

if __name__ == "__main__":
    cb = CircularBuffer2()
    #cb.get()
    for i in range(3):
       for j in range(3):
           cb.add(j)
           print("Added: " + str(j))
           print("Head: " + str(cb.get_head()) + "   Tail: " + str(cb.get_tail()))
           print("Free: " + str(cb.get_free_space()) + " Occupied: " + str(cb.get_occupied_space()))

       for j in range(3):
           print("Got: " + str(cb.get()))
           print("Head: " + str(cb.get_head()) + "   Tail: " + str(cb.get_tail()))
           print("Free: " + str(cb.get_free_space()) + " Occupied: " + str(cb.get_occupied_space()))

