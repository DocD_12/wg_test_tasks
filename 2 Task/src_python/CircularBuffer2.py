class CircularBuffer2:
    def __init__(self):
        self.size = 8
        self.data = [None] * self.size
        self.head = 0
        self.tail = 0
        self.mask = self.size - 1
        self.full = 0

    def get_head(self):
        return self.head

    def get_tail(self):
        return self.tail

    def get_free_space(self):
        if self.full:
            return 0
        else:
            return self.size - ((self.head - self.tail) & self.mask)

    def get_occupied_space(self):
        if self.full:
            return self.size
        else:
            return ((self.head - self.tail) & self.mask)

    def add(self, value):
        if (self.full):
            print(self.full)
            raise MemoryError("CircularBuffer2 is Full")
        self.data[self.head] = value
        self.head = (self.head + 1) & self.mask
        if (self.head == self.tail):
            self.full = 1

    def get(self):
        if (self.head == self.tail) and not self.full:
            raise MemoryError("CircularBuffer2 is Empty")
        res = self.data[self.tail]
        self.tail = (self.tail + 1) & self.mask
        if (self.head != self.tail):
            self.full = 0
        return res
