#ifndef CB2_H_
#define CB2_H_
#include <iostream>
  
class CircularBuffer2 {
private:
    int*    m_data;          
    int     m_size;
    int     m_head;
    int     m_tail;
    int 	m_mask;
    bool   	m_full;
 
public:
    CircularBuffer2();
    ~CircularBuffer2();

    int GetHead();
    int GetTail();
    int GetFreeSpace();
    int GetOccupiedSpace();
    void Add(int val);
    int Get();
};

#endif
