#ifndef CB_H_
#define CB_H_
#include <iostream>
  
class CircularBuffer {
private:
    int*    m_data;          
    int     m_size;
    int     m_head;
    int     m_tail;
    int Head_Tail();
    int PointIncrement(int point);

public:
    CircularBuffer();
    CircularBuffer(int con_size);
    ~CircularBuffer();

    int GetHead();
    int GetTail();
    int GetFreeSpace();
    int GetOccupiedSpace();
    void Add(int val);
    int Get();

};

#endif
