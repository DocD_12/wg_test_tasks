#include <iostream>
#include "CircularBuffer.h"
#include "CircularBuffer2.h"

using namespace std;

int main(int argc, char **argv)
{

	CircularBuffer2 c;

	for (int j = 3; j >= 0; j--)
	{
		for (int i = 1; i <= 3; ++i)
		{
			c.Add(i);
			cout << "Added: "<< i << endl;
			cout << "Free space: " << c.GetFreeSpace() << "; Occupied: " << c.GetOccupiedSpace() << endl;
		}

		for (int i = 0; i < 3; ++i)
		{
			cout << "Got: " << c.Get() << endl;
			cout << "Free space: " << c.GetFreeSpace() << "; Occupied: " << c.GetOccupiedSpace() << endl;
		}

	}
    return 0;
}
