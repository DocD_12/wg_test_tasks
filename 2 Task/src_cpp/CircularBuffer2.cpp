#include <iostream>
#include "CircularBuffer2.h"

using namespace std;
 
CircularBuffer2::CircularBuffer2()
{
    m_size = 8;
    m_data = new int[m_size];
    m_head = 0;
    m_tail = 0;
    m_mask = m_size - 1;
    m_full = 0;
}
 
CircularBuffer2::~CircularBuffer2()
{ delete[] m_data; }

int CircularBuffer2::GetHead()
{
	return m_head;
}

int CircularBuffer2::GetTail()
{
	return m_tail;
}
 
int CircularBuffer2::GetFreeSpace()
{
	if (m_full)	return 0;
	return m_size - ((m_head - m_tail) & m_mask);
}

int CircularBuffer2::GetOccupiedSpace()
{
	if (m_full) return m_size;
	return (m_head - m_tail) & m_mask;
}

void CircularBuffer2::Add(int val)
{
	if (m_full)
		throw "Buffer is full";
	m_data[m_head++] = val;
	m_head &= m_mask;
	if (m_head == m_tail) m_full = 1;
}
 
int CircularBuffer2::Get()
{

    if ((m_head == m_tail) && !m_full)
        throw "Buffer is empty";
    int res = m_data[m_tail++];
    m_tail &= m_mask;
    if (m_tail != m_head) m_full = 0;
    return res;
}
