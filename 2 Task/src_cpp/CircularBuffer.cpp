#include <iostream>
#include "CircularBuffer.h"

using namespace std;



CircularBuffer::CircularBuffer()
{
    m_size = 5;
    m_data = new int[m_size];
    m_head = 0;
    m_tail = 0;
}

CircularBuffer::CircularBuffer(int con_size)
{
    m_size = con_size;
    m_data = new int[m_size];
    m_head = 0;
    m_tail = 0;
}

int CircularBuffer::GetHead()
{
	return m_head;
}


int CircularBuffer::GetTail()
{
	return m_tail;
}

CircularBuffer::~CircularBuffer()
{ 
    delete[] m_data; 
}
 
int CircularBuffer::GetFreeSpace()
{
	return m_size - Head_Tail();
}

int CircularBuffer::GetOccupiedSpace()
{
	return Head_Tail();
}

void CircularBuffer::Add(int val)
{
    m_data[m_head] = val;
    m_head = PointIncrement(m_head);
    if (m_head == m_tail) m_tail = PointIncrement(m_tail);
}
 
int CircularBuffer::Get()
{

    if (m_head == m_tail)
        throw "Buffer is empty";
    int res = m_data[m_tail];
	m_tail = PointIncrement(m_tail);
    return res;
}

int CircularBuffer::Head_Tail()
{
	if (m_head < m_tail) return (m_head + m_size - m_tail);
	return m_head - m_tail;
}

int CircularBuffer::PointIncrement(int point)
{
	if (point++ >= m_size) return 0;
	return point;
}
