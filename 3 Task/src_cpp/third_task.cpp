#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <math.h>
using namespace std;

void insertionSort(int arr[], int left, int right)
{
    for (int i = left + 1; i <= right; i++)
    {
        int temp = arr[i];
        int j = i - 1;
        while (j >= left && arr[j] > temp)
        {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = temp;
    }
}

void merge(int arr[], int l, int m, int r)
{
    int len1 = m - l + 1, len2 = r - m;
    int left[len1], right[len2];
    for (int i = 0; i < len1; i++)
        left[i] = arr[l + i];
    for (int i = 0; i < len2; i++)
        right[i] = arr[m + 1 + i];

    int i = 0;
    int j = 0;
    int k = l;

    while (i < len1 && j < len2)
    {
        if (left[i] <= right[j])
        {
            arr[k] = left[i];
            i++;
        }
        else
        {
            arr[k] = right[j];
            j++;
        }
        k++;
    }

    while (i < len1)
    {
        arr[k] = left[i];
        k++;
        i++;
    }

    while (j < len2)
    {
        arr[k] = right[j];
        k++;
        j++;
    }
}
int minRun(int n)
{
	int r = 0;
	while (n >= 64) {
		r |= n & 1;
		n >>= 1;
	}
	return n + r;
}
void timsort(int* arr, int n)
{
	int RUN = minRun(n);

    for (int i = 0; i < n; i+=RUN)
        insertionSort(arr, i, min((i+RUN-1), (n-1)));

    for (int size = RUN; size < n; size = 2*size)
    {
        for (int left = 0; left < n; left += 2*size)
        {
            int mid = left + size - 1;
            int right = min((left + 2*size - 1), (n-1));

            if(mid < right) merge(arr, left, mid, right);
        }
    }
}
void show_array(int* arr, int len)
{
	int count = 0;
	int line = (int) sqrt(len);
	for (int i = 0; i < line; i++)
	{
		for (int j = 0; j < len / line; j++)
		{
			cout << setw(5) << arr[count];
			count ++;
		}
		cout << endl;
	}
	cout << endl;
}

int main()
{
	const int arr_len = 1024;
	int arr[arr_len];
	for (int i = 0; i < arr_len; i++)
	{
		arr[i] = rand() % arr_len;
	}

	show_array(arr, arr_len);

	timsort(arr,  arr_len);

	show_array(arr, arr_len);

	return 0;
}
