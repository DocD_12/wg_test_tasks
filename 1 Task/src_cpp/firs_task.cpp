#include <iostream>

bool isEven(int value){return !(value & 1);}

int main(int argc, char **argv)
{
	int number;
    printf("Enter integer number for isEven check: ");
    scanf("%d", &number);
    if (isEven(number))
		printf("Number is even");
	else
		printf("Number is odd");
    
	return 0;
}

