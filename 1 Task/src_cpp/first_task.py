def isEven(value):return bool(not (value & 1))

if __name__ == "__main__":
    num = int(input("Enter integer number for isEven check: "))
    print(isEven(num))
